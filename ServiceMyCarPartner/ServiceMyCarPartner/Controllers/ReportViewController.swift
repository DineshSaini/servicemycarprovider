//
//  ReportViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 01/07/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import WebKit

class ReportViewController: UIViewController,WKNavigationDelegate,UIScrollViewDelegate{
    
     var webview : WKWebView!
    
    var url = ""
    
    override func loadView() {
        webview = WKWebView()
        webview.navigationDelegate = self
        view = webview
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //webview.delegate = self
       // webview.scalesPageToFit = true
        self.webview.scrollView.alwaysBounceHorizontal = false
        self.webview.scrollView.alwaysBounceVertical = false
        self.webview.scrollView.showsHorizontalScrollIndicator = false
        self.webview.scrollView.showsVerticalScrollIndicator = false
        //self.webview.scrollView.bounces = false
        self.webview.scrollView.bouncesZoom = false
        if url != ""{
        let req = URL(string: url)
        let webUrl = URLRequest(url: req!)
            self.webview.load(webUrl)
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackBarButton(_sender:UIButton){
        self.navigationController?.pop(true)
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CommonClass.hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CommonClass.hideLoader()
    }

    
    
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        if url != ""{return}
//        if !CommonClass.isLoaderOnScreen{
//            CommonClass.showLoader()
//        }
//    }
//
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        if url != ""{return}
//
//        if CommonClass.isLoaderOnScreen{
//            CommonClass.hideLoader()
//        }
//    }
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        if url != ""{return}
//
//        if CommonClass.isLoaderOnScreen{
//            CommonClass.hideLoader()
//        }
//
//    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

