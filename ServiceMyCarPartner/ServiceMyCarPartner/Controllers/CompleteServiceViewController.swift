//
//  CompleteServiceViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit

class CompleteServiceViewController: UIViewController {
    
    
    @IBOutlet weak var completeTableView:UITableView!
    
    var isNewDataLoading:Bool = false
    var dataLoading:Bool = true
    
    let recordPerPage = 10
    var pageNumber = 1
    var user : User!
    var orders = Array<Order>()

    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = appBackGroundColor
        refreshControl.addTarget(self, action: #selector(CompleteServiceViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.completeTableView.dataSource = self
        self.completeTableView.delegate = self
        self.user = User.loadSavedUser()

        let orderNib = UINib(nibName: "ServiceTableViewCell", bundle: nil)
        self.completeTableView.register(orderNib, forCellReuseIdentifier: "ServiceTableViewCell")
        let noOrderNib = UINib(nibName: "NoDataTableViewCell", bundle: nil)
        self.completeTableView.register(noOrderNib, forCellReuseIdentifier: "NoDataTableViewCell")
        //self.completeTableView.addSubview(refreshControl)
        //self.completeTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.completeTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.orderBookingList()
    }
    
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.pageNumber = 1
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        AppServices.sharedInstance.orderListFromServer(driverId: self.user.userID, status: "p") { (success, result, message) in
            self.isNewDataLoading = true
            refreshControl.endRefreshing()
            if let orderResult = result{
                self.orders.removeAll()
                self.orders.append(contentsOf:orderResult)
                self.completeTableView.reloadData()
            }
        }
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == completeTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
                if CommonClass.isConnectedToNetwork{
                    self.orderBookingList()
                    self.completeTableView.reloadData()
                }
            }
        }
    }
    
    
    func orderBookingList(){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        AppServices.sharedInstance.orderListFromServer(driverId: self.user.driverID, status: "c") { (success, result, message) in
            if let orderResult = result{
                self.orders.removeAll()
                self.orders.append(contentsOf:orderResult)
                self.completeTableView.reloadData()
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CompleteServiceViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orders.count == 0) ? 1 : orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if orders.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceTableViewCell", for: indexPath) as! ServiceTableViewCell
            let order = orders[indexPath.row]
            if order.jobType == "C"{
                cell.address.text = "COLLECTION : \(order.address)"
            }else{
                cell.address.text = "DROP : \(order.address)"
            }
            cell.serviceType.text = order.serviceType.capitalized
            cell.orderID.text = order.orderId.capitalized
            cell.jobType.text = order.jobTypeTxt.capitalized
            cell.dateAndTime.text = order.dateTime
            cell.status.text = (order.tripStatus == "P") ? "Pending" : ((order.tripStatus == "S") ? "Start" : "Complete")
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if orders.count != 0{
            let order = orders[indexPath.row]
            let orderDetailVC = AppStoryboard.Dashboard.viewController(ServiceOrderDetailViewController.self)
            orderDetailVC.order = order
            orderDetailVC.isStart = true
            orderDetailVC.isJobStart = true
            orderDetailVC.isJobComplete = true
            self.navigationController?.pushViewController(orderDetailVC, animated: true)
        }
    }}

