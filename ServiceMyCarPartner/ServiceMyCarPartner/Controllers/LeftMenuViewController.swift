//
//  LeftMenuViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import StoreKit

class LeftMenuViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var slideOutAnimationEnabled : Bool!
    var fromMenu = false
    let leftMenuOptions = ["","Service",/*"Terms & Condition",*/"Privacy Policy","Logout"]
    
    var isRestaurentTrue = false
    
    override func viewDidLoad() {
        slideOutAnimationEnabled = false
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame:CGRect())
        SlideNavigationController.sharedInstance().enableShadow = false
        SlideNavigationController.sharedInstance().enableSwipeGesture = false
        SlideNavigationController.sharedInstance().portraitSlideOffset = UIScreen.main.bounds.size.width * 1 / 5
        SlideNavigationController.sharedInstance().avoidSwitchingToSameClassViewController = false
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
 
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView != nil{
            self.tableView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return leftMenuOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        if indexPath.row == 0{
            height = 140
        }else{
            height = 60
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.row == 0{
            cell = profileCell(tableView, cellForRowAt: indexPath)
        }else{
            cell = regularCell(tableView, cellForRowAt: indexPath)
        }
        return cell
    }
    
    func profileCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> ProfileLeftMenuCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileLeftMenuCell", for: indexPath) as! ProfileLeftMenuCell
        cell.selectionStyle = .none
//        CommonClass.makeViewCircular(cell.imageIconView, borderColor: UIColor.clear, borderWidth: 0.0)
//        cell.imageIconView.sd_setImage(with: URL(string:self.user.profileImage), placeholderImage: #imageLiteral(resourceName: "edit_profile"))
//        cell.nameLabel.text = self.user.userName.capitalized
//        cell.emailLabel.text = "CPR NO. : \(self.user.cprNumber)"
        return cell
    }
    func regularCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> LeftMenuTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath) as! LeftMenuTableViewCell
        cell.selectionStyle = .none
        cell.nameLabel.text = leftMenuOptions[indexPath.row]
        return cell
    }
    
    
    
    
    func addShadow(view:UIView){
        view.layer.cornerRadius = 2.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.white.withAlphaComponent(0.8).cgColor
        view.layer.masksToBounds = true
        
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 2, height: -2)
        view.layer.shadowRadius = 1.0
        view.layer.shadowOpacity = 1.0
        view.layer.masksToBounds = true
        view.layer.shadowPath = UIBezierPath(roundedRect: view.frame, cornerRadius:1).cgPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var vc : UIViewController!
        switch indexPath.row{
        case 0:
            return
        case 1:
            vc = AppStoryboard.Dashboard.viewController(ServiceViewController.self)
        case 2:
             vc = AppStoryboard.Dashboard.viewController(TermsAndConditionViewController.self)
        case 3:
            askForLogout()
        default:
            break;
        }
        if vc != nil{
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
        }
        //        else{
        //            SlideNavigationController.sharedInstance().closeMenu(completion: nil)
        //        }
        
    }
    
    func shareApplication(){
        SlideNavigationController.sharedInstance().closeMenu {
            let textToShare = "Check this application, you will definitily get some benifit!"
            let objectsToShare = [textToShare]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
            appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
        }
    }
    
    
    
    func showAlertView(){
        let alert = UIAlertController(title: ALERT_TITTLE_MESSAGE, message: FUNCTIONALITY_PENDING_MESSAGE, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    func askForLogout() {
        //SlideNavigationController.sharedInstance().closeMenu {
        let alert = UIAlertController(title: "Would you really want to logout?", message: nil, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.logout()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        //}
    }
    
    func logout() {
        logoutFromServer()
        //        let user = User()
        //        user.saveUserInfo(user)
        self.updateCookies()
        //       for key in kUserDefaults.dictionaryRepresentation().keys {
        //            if key == kAppToken{
        //                //do nothing
        //            }else{
        //kUserDefaults.removeObject(forKey: key)
        //            }
        //        }
    }
    
    func logoutFromServer(){
        
        
        kUserDefaults.set(false, forKey: kIsLoggedIN)
    //    User.deleteUserOnLogout()
        SlideNavigationController.sharedInstance().closeMenu {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController = nav
        
//        LoginService.sharedInstance.logOut { (success, user, message) in
//            if success == true{
//                kUserDefaults.set(false, forKey: kIsLoggedIN)
//                User.deleteUserOnLogout()
//                SlideNavigationController.sharedInstance().closeMenu {
//                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let nav = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
//                    let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
//                    appDelegate.window?.rootViewController = nav
//                }
//                //showSuccessWithMessage("You logout Successfully!")
//            }
        }
    }
    
    func updateCookies() {
        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.removeCookies(since: Date(timeIntervalSinceReferenceDate: 1))
    }
    
}


class LeftMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageIconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ProfileLeftMenuCell: UITableViewCell {
    @IBOutlet weak var imageIconView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    //    CommonClass.makeViewCircular(imageIconView, borderColor: .clear, borderWidth: 0.0)
        
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}






