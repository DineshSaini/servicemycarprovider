//
//  PendingServiceViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
let colorGreen = UIColor.init(red: 35.0/255.0, green: 158.0/255.0, blue: 0.0/255.0, alpha: 1.0)
class PendingServiceViewController: UIViewController {
    
    @IBOutlet weak var pendingTableView:UITableView!
    
    var isNewDataLoading:Bool = false
    var dataLoading:Bool = true
    
    let recordPerPage = 10
    var pageNumber = 1
    var orders = Array<Order>()
    var user : User!
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = appBackGroundColor
        refreshControl.addTarget(self, action: #selector(PendingServiceViewController.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.pendingTableView.dataSource = self
        self.pendingTableView.delegate = self
        self.user = User.loadSavedUser()
        let orderNib = UINib(nibName: "ServiceTableViewCell", bundle: nil)
        self.pendingTableView.register(orderNib, forCellReuseIdentifier: "ServiceTableViewCell")
        let noOrderNib = UINib(nibName: "NoDataTableViewCell", bundle: nil)
        self.pendingTableView.register(noOrderNib, forCellReuseIdentifier: "NoDataTableViewCell")
       // self.pendingTableView.addSubview(refreshControl)
        //self.pendingTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.pendingTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.orderBookingList()
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.pageNumber = 1
        if !CommonClass.isConnectedToNetwork{
            refreshControl.endRefreshing()
            showErrorWithMessage(NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        AppServices.sharedInstance.orderListFromServer(driverId: self.user.userID, status: "p") { (success, result, message) in
            self.isNewDataLoading = true
            refreshControl.endRefreshing()
            if let orderResult = result{
                self.orders.removeAll()
                self.orders.append(contentsOf:orderResult)
                self.pendingTableView.reloadData()
            }
        }
       }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == pendingTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
                    if CommonClass.isConnectedToNetwork{
                        self.orderBookingList()
                        self.pendingTableView.reloadData()
                }
            }
        }
    }

    
    func orderBookingList(){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        AppServices.sharedInstance.orderListFromServer(driverId: self.user.driverID, status: "p") { (success, result, message) in
            if let orderResult = result{
                self.orders.removeAll()
                self.orders.append(contentsOf:orderResult)
                self.pendingTableView.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PendingServiceViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orders.count == 0) ? 1 : orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if orders.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceTableViewCell", for: indexPath) as! ServiceTableViewCell
            let order = orders[indexPath.row]
            if order.jobType == "C"{
             cell.address.text = "COLLECTION : \(order.address)"
            }else{
             cell.address.text = "DROP : \(order.address)"
            }
            cell.serviceType.text = order.serviceType.capitalized
            cell.orderID.text = order.orderId.capitalized
            cell.jobType.text = order.jobTypeTxt.capitalized
            cell.dateAndTime.text = order.dateTime
            cell.status.text = (order.tripStatus == "P") ? "Pending" : ((order.tripStatus == "S") ? "Start" : "Complete")

        return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if orders.count != 0{
        let order = orders[indexPath.row]
        let orderDetailVC = AppStoryboard.Dashboard.viewController(ServiceOrderDetailViewController.self)
            orderDetailVC.order = order
            if order.tripStatus == "S"{
                orderDetailVC.isStart = true
            }
        self.navigationController?.pushViewController(orderDetailVC, animated: true)
        }
    }
}
