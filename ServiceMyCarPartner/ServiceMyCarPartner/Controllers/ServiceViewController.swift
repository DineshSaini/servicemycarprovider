//
//  ServiceViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit

class ServiceViewController: UIViewController {
    
    @IBOutlet weak var pendingButton:UIButton!
    @IBOutlet weak var completeButton:UIButton!
    @IBOutlet weak var badgeCountLabel:UILabel!

    @IBOutlet weak var scrolling:UIScrollView!
    @IBOutlet weak var movingView:UIView!

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrolling.isPagingEnabled = true
        self.scrolling.bounces = false
        self.configureButtonTextColor()
        toggleRateButton(button: pendingButton)
        self.badgeCountLabel.isHidden = true
        let todayVc = AppStoryboard.Dashboard.viewController(PendingServiceViewController.self)
        self.addChild(todayVc)
        let completeVc = AppStoryboard.Dashboard.viewController(CompleteServiceViewController.self)
        self.addChild(completeVc)
        CommonClass.makeViewCircular(badgeCountLabel, borderColor: .clear, borderWidth: 0.0)
        makeCircularWithTopTwoCornerRadius(pendingButton, cornerRadius: 15.0)
        makeCircularWithTopTwoCornerRadius(completeButton, cornerRadius: 15.0)

        self.perform(#selector(LoadScollView), with: nil, afterDelay: 0.5)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func tappedOnMenuButton(_ sender: UIButton){
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }

    
    
    func configureButtonTextColor(){
        pendingButton.setTitleColor(.black, for: .selected)
        pendingButton.setTitleColor(.black, for: .normal)
        completeButton.setTitleColor(.black, for: .selected)
        completeButton.setTitleColor(.black, for: .normal)
        
    }
    
    
    func toggleRateButton(button:UIButton){
        if button == completeButton{
            pendingButton.isSelected = false
            pendingButton.backgroundColor = UIColor.white
            pendingButton.setTitleColor(UIColor.white, for: .selected)
            completeButton.setTitleColor(UIColor.white, for: .selected)
            pendingButton.setTitleColor(UIColor.black, for: .normal)
            completeButton.setTitleColor(UIColor.black, for: .normal)
            completeButton.isSelected = true
            completeButton.backgroundColor = colorGreen
            
        }else {
            pendingButton.setTitleColor(UIColor.white, for: .selected)
            completeButton.setTitleColor(UIColor.white, for: .selected)
            pendingButton.setTitleColor(UIColor.black, for: .normal)
            completeButton.setTitleColor(UIColor.black, for: .normal)
            completeButton.isSelected = false
            completeButton.backgroundColor = UIColor.white
            pendingButton.isSelected = true
            pendingButton.backgroundColor = colorGreen
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

let kScreenWidth = UIScreen.main.bounds.size.width

extension ServiceViewController:UIScrollViewDelegate{
    
    @IBAction func onClickPendingButton(_ sender: UIButton){
        self.toggleRateButton(button: pendingButton)
        self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
     //   self.raceTo(CGPoint(x:self.pendingButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
        
    }
    
    
    @IBAction func onClickCompletedButton(_ sender: UIButton){
        self.toggleRateButton(button: completeButton)
        self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
      //  self.raceTo(CGPoint(x:self.completeButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
        
    }
    
    
    @objc func LoadScollView() {
        scrolling.delegate = nil
        scrolling.contentSize = CGSize(width:kScreenWidth * 2, height:scrolling.frame.size.height)
        for i in 0 ..< self.children.count {
            self.loadScrollViewWithPage(i)
        }
        scrolling.delegate = self
    }
    
    
    func loadScrollViewWithPage(_ page: Int) {
        if page < 0 {
            return
        }
        if page >= self.children.count {
            return
        }
        var pendingVC : PendingServiceViewController
        var completeVC : CompleteServiceViewController
        var frame: CGRect = scrolling.frame
        switch page {
        case 0:
            pendingVC = self.children[page] as! PendingServiceViewController
            pendingVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            pendingVC.view.frame = frame
            scrolling.addSubview(pendingVC.view!)
            pendingVC.view.setNeedsLayout()
            pendingVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            pendingVC.view.setNeedsLayout()
            pendingVC.view.layoutIfNeeded()
            
        case 1:
            completeVC = self.children[page] as! CompleteServiceViewController
            completeVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            completeVC.view.frame = frame
            scrolling.addSubview(completeVC.view!)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            completeVC.view.setNeedsLayout()
            completeVC.view.layoutIfNeeded()
            
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageCurrent = Int(floor(scrolling.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleRateButton(button: pendingButton)
            self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
           // self.raceTo(CGPoint(x:self.pendingButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        case 1:
            self.toggleRateButton(button: completeButton)
            self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
           // self.raceTo(CGPoint(x:self.completeButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        default:
            break
        }
    }
    func raceTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
        var stopPoint: CGPoint = destination
        if withSnapBack {
            let diffx = destination.x - movingView.frame.origin.x
            let diffy = destination.y - movingView.frame.origin.y
            if diffx < 0 {
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                stopPoint.x += 10.0
            }
            
            if diffy < 0 {
                stopPoint.y -= 10.0
            }
            else if diffy > 0 {
                stopPoint.y += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        movingView.frame = CGRect(x:stopPoint.x, y:stopPoint.y, width: movingView.frame.size.width,height: movingView.frame.size.height)
        UIView.commitAnimations()
        let firstDelay = 0.1
        let startTime = firstDelay * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + startTime) {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            self.movingView.frame = CGRect(x:destination.x, y:destination.y,width: self.movingView.frame.size.width, height: self.movingView.frame.size.height)
            UIView.commitAnimations()
        }
    }
    
    
    func raceScrollTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - scrolling.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        if isleft {
            scrolling.contentOffset = CGPoint(x:destination.x - 5, y:destination.y)
        }
        else {
            scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
        }
        
        UIView.commitAnimations()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            if isleft {
                self.scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
            }
            else {
                self.scrolling.contentOffset = CGPoint(x:destination.x - 5,y: destination.y)
            }
            UIView.commitAnimations()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0){() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.easeInOut)
                self.scrolling.contentOffset = CGPoint(x:destination.x, y:destination.y)
                UIView.commitAnimations()
                
            }
        }
    }
}


extension ServiceViewController{
    
    
    func makeCircularWithLeftBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner,.layerMaxXMinYCorner]
    }
    
    func makeCircularWithRightBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }
    
    
    func makeCircularWithLeftTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func makeCircularWithRightTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner,.layerMinXMinYCorner]
        
    }
    
    
    func makeCircularWithLeftTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func makeCircularWithRightTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        
    }
    
    
    
    func makeCircularWithTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]// [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }
    
}
