//
//  ServiceOrderDetailViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase

class ServiceOrderDetailViewController: UIViewController {
    
    
    @IBOutlet weak var orderTableView:UITableView!
    @IBOutlet weak var mapView:GMSMapView!
    
    var order = Order()
    var user:User!
    
    var isJobStart:Bool = false
    var isStart:Bool = false
    var isJobComplete = false
    
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var previousLocation: CLLocation?
    var previousLatitude = 0.0
    var previousLongitude = 0.0
    var didFindMyLocation = false
    var carMarker = GMSMarker()
    var dropMarker = GMSMarker()
    var isMarkerInitialized = false
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var traveledDistance: Double = 0
    var moveMent: ARCarMovement!
    var markerHeading:CLHeading!
    
    
    var ref: DatabaseReference!
    var rideRequestRef: DatabaseReference!
    var newRideRequestHandle: DatabaseHandle?
    
    var changedRideRequestHandle: DatabaseHandle?
    var removeRideRequestHandle: DatabaseHandle?
    
    var _refHandle: DatabaseHandle!
    var rootHandle: DatabaseHandle?
    var childHandle: DatabaseHandle!
    
    
    
    var driverRemoveRef: DatabaseReference!
    var newDriverRemoveHandle: DatabaseHandle?
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User.loadSavedUser()
        self.orderTableView.dataSource = self
        self.orderTableView.delegate = self
        self.referenceHandle()
        self.ref = AppDelegate.getDatabaseRef()
        self.ref.keepSynced(false)
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        self.setupMapView()
        let orderNib = UINib(nibName: "OrderDetailTableViewCell", bundle: nil)
        self.orderTableView.register(orderNib, forCellReuseIdentifier: "OrderDetailTableViewCell")
        let jobNib = UINib(nibName: "StartAndEndJobTableViewCell", bundle: nil)
        self.orderTableView.register(jobNib, forCellReuseIdentifier: "StartAndEndJobTableViewCell")
        let noOrderNib = UINib(nibName: "NoDataTableViewCell", bundle: nil)
        self.orderTableView.register(noOrderNib, forCellReuseIdentifier: "NoDataTableViewCell")
        NotificationCenter.default.addObserver(self, selector: #selector(ServiceOrderDetailViewController.appDelegateLocationUpdate(_:)), name: .APPDELEGATE_UPDATE_LOCATION_NOTIFICATION, object: nil)
        self.mapViewLoad()
        self.orderTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func referenceHandle(){
        if let refHandle = _refHandle {
            self.ref.removeObserver(withHandle: refHandle)
        }
    }
    
    
    @objc func appDelegateLocationUpdate(_ notification: Notification){
        if let locationInfo = notification.userInfo as? [String:AnyObject]{
            if let location = locationInfo["currentLocation"] as? CLLocation{
                self.currentLocation = location
                startLocation = location
                if !isMarkerInitialized{
                    self.initializeMarker(center: location.coordinate)
                }else{
                    self.mapView.animate(toLocation: location.coordinate)
                }
                self.getCurrentDistace(lastLocation:location)
                self.locationUpdate()
            }
        }
    }
    
    
    func mapViewLoad(){
        if AppDelegate.getAppDelegate().currentLocation != nil{
            self.currentLocation = AppDelegate.getAppDelegate().currentLocation
            startLocation = AppDelegate.getAppDelegate().currentLocation
            if !isMarkerInitialized{
                self.initializeMarker(center: AppDelegate.getAppDelegate().currentLocation.coordinate)
            }else{
                self.mapView.animate(toLocation: AppDelegate.getAppDelegate().currentLocation .coordinate)
            }
            self.locationUpdate()
        }
    }
    
    
    
    func setupMapView(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        locationManager.distanceFilter = 0.0
        moveMent = ARCarMovement()
        moveMent.delegate = self
        mapView.delegate = self
        self.locationUpdate()
        
        
    }


    

    @IBAction func onClickBackBarButton(_ sender:UIButton){
        self.navigationController?.pop(true)
    }
    
    @IBAction func onClickCheckAndCompleteButton(_ sender: UIButton){
        self.endJobByProvider()
        
    }

    
    @IBAction func onClickCallCustomerButton(_ sender: UIButton){
        if order.customerPhone == ""{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Customer Contact not available!")
            return
        }
        if let url = URL(string: "tel://\(order.customerPhone )"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onClickCallAdminButton(_ sender: UIButton){
        if order.adminPhone == ""{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Admin Contact not available!")
            return
        }
        let replaceNumber = order.adminPhone.replacingOccurrences(of: "(", with: "")
        let adminNumber = replaceNumber.replacingOccurrences(of: ")", with: "")
        let removeSpaceNumber = adminNumber.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://+\(removeSpaceNumber)"),UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }


    
    @IBAction func onClickGooglemapButton(_ sender: UIButton){
        let source = "\(AppDelegate.getAppDelegate().currentLocation.coordinate.latitude),\(AppDelegate.getAppDelegate().currentLocation.coordinate.longitude)"
        let destination = "\(self.order.startLat),\(self.order.startLong)"
        let urlString = "comgooglemaps://?saddr=\(source)&daddr=\(destination)&directionsmode=driving"
        if let UrlNavigation = URL(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if (source != "" && destination != "") {
                    if let urlDestination = URL(string:urlString) {
                        UIApplication.shared.open(urlDestination, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }else {
                if let urlDestination = URL(string: "https://www.google.co.in/maps/dir/?saddr=\(source)&daddr=\(destination)&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        }else{
            if let urlDestination = URL(string: "https://www.google.co.in/maps/dir/?saddr=\(source)&daddr=\(destination)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }
    
    
    func endJobByProvider(){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        CommonClass.showLoader(withStatus: "Wait..")
        AppServices.sharedInstance.endJobFromServer(orderID: self.order.intId) { (success, result, message) in
            CommonClass.hideLoader()
            if message != ""{
                self.isJobStart = true
                self.isJobComplete = true
                self.removeDriverFromFirebase()
                self.orderTableView.reloadData()
                 NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Job Completed Successfully")
                
            }
        }
    }
    
    
    func startJobByProvider(){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        let latitude = AppDelegate.getAppDelegate().currentLocation.coordinate.latitude
        let longitude = AppDelegate.getAppDelegate().currentLocation.coordinate.longitude

        CommonClass.showLoader(withStatus: "Wait..")
        AppServices.sharedInstance.startJobFromServer(orderID: self.order.intId, lat: "\(latitude)", long: "\(longitude)") { (success, result, message) in
            CommonClass.hideLoader()
            if message != ""{
                self.isStart = true
                self.orderTableView.reloadData()
                NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Job Started Successfully")

            }
        }
    }
    
    @IBAction func onClickEndJobButton(_ sender:UIButton){
        self.viewReportFromServer(self.order.orderId, orderType: order.jobType)
    }
    
    @IBAction func onClickStartJobButton(_ sender:UIButton){
        self.startJobByProvider()
    }
    
    
    func viewReportFromServer(_ orderID:String, orderType:String){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        CommonClass.showLoader(withStatus: "Wait..")
        AppServices.sharedInstance.webReportFromServer(orderID: orderID, type: orderType) { (success, result, message) in
            CommonClass.hideLoader()
            if let orderData = result{
                let reportVc = AppStoryboard.Dashboard.viewController(ReportViewController.self)
                reportVc.url = orderData.url
                self.navigationController?.pushViewController(reportVc, animated: true)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ServiceOrderDetailViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 200
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTableViewCell", for: indexPath) as! OrderDetailTableViewCell
        if order.jobType == "C"{
            cell.address.text = "COLLECTION : \(order.address)"
        }else{
            cell.address.text = "DROP : \(order.address)"
        }
        cell.serviceType.text = order.serviceType.capitalized
        cell.orderID.text = order.orderId.capitalized
        cell.jobType.text = order.jobTypeTxt.capitalized
        cell.dateAndTime.text = order.dateTime
        cell.carDetail.text = "\(order.varYear) \(order.varCar) \(order.varModel)"
        cell.clientButton.addTarget(self, action: #selector(onClickCallAdminButton(_:)), for: .touchUpInside)
        cell.customerButton.addTarget(self, action: #selector(onClickCallCustomerButton(_:)), for: .touchUpInside)
            if isStart{
                cell.checkCompleteButton.setTitle("End Job", for: .normal)
                cell.checkCompleteButton.addTarget(self, action: #selector(onClickCheckAndCompleteButton(_:)), for: .touchUpInside)
            }else{
                cell.checkCompleteButton.setTitle("Start Job", for: .normal)
                cell.checkCompleteButton.addTarget(self, action: #selector(onClickStartJobButton(_:)), for: .touchUpInside)
            }
            if isJobStart{
                cell.checkCompleteButton.isHidden = true
            }
        return cell
        }else{
          let cell = tableView.dequeueReusableCell(withIdentifier: "StartAndEndJobTableViewCell", for: indexPath) as! StartAndEndJobTableViewCell
            if !isJobStart{
                cell.startJobButton.isHidden = true
                cell.endJobButton.isHidden = true

            }
            if isJobStart{
                cell.endJobButton.isHidden = false
                cell.startJobButton.isHidden = true
            }
            cell.endJobButton.addTarget(self, action: #selector(onClickEndJobButton(_:)), for: .touchUpInside)
            return cell
        }
    }
}



extension ServiceOrderDetailViewController:CLLocationManagerDelegate,GMSMapViewDelegate,ARCarMovementDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
            
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
            
            break
        case .restricted:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if !isEnabling{
                    return
                }
            }
            break
        case .denied:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: self){ isEnabling in
                if !isEnabling{
                    return
                }
            }
            break
        }
    }
    
    func initializeMarker(center:CLLocationCoordinate2D){
        self.isMarkerInitialized=true
        self.setUpMap()
        mapView.camera = GMSCameraPosition(target: center, zoom: 15.0, bearing: 0, viewingAngle: 0)
    }
    
    func animateMapToCenter(position:CLLocationCoordinate2D){
        let camera = GMSCameraPosition.camera(withTarget: position, zoom: 15.0)
        mapView.camera = camera
        mapView.animate(to: camera)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
        }
        if let location = locations.first{
            if isMarkerInitialized{
                self.mapView.animate(toLocation:currentLocation.coordinate)
                self.locationUpdate()
            }else{
                self.initializeMarker(center:currentLocation.coordinate)
            }
        }
    }
    
    
    func setUpMap(){
        if currentLocation != nil{
            self.isMarkerInitialized=true
            let pickCoordinate = CLLocationCoordinate2D(latitude: AppDelegate.getAppDelegate().currentLocation.coordinate.latitude, longitude: AppDelegate.getAppDelegate().currentLocation.coordinate.longitude)
            let dropCordinate = CLLocationCoordinate2D(latitude: Double(self.order.startLat) ?? 0.0, longitude: Double(self.order.startLong) ?? 0.0)
            addMarkerForPickUpAndDropOffAddress(pickupCoordinate: pickCoordinate, dropOffCoordinate: dropCordinate)
            resetBoundOfMap(pickCoordinate: pickCoordinate, dropCoordinate: dropCordinate)
            //self.drawPath()
        }
    }
    func resetBoundOfMap(pickCoordinate:CLLocationCoordinate2D,dropCoordinate:CLLocationCoordinate2D) -> Void {
        let path = GMSMutablePath()
        path.add(pickCoordinate)
        path.add(dropCoordinate)
        let bounds = GMSCoordinateBounds(path: path)
        let camera = mapView.camera(for: bounds, insets:UIEdgeInsets(top: 40, left: 40, bottom: 50, right: 40))
        mapView.camera = camera!;
        
    }
    
    func addMarkerForPickUpAndDropOffAddress(pickupCoordinate:CLLocationCoordinate2D,dropOffCoordinate:CLLocationCoordinate2D) -> Void {
        isMarkerInitialized = true
        self.carMarker = GMSMarker(position: pickupCoordinate)
        carMarker.icon = #imageLiteral(resourceName: "car_map")
        carMarker.isTappable = true
        carMarker.map = self.mapView
        
        self.dropMarker = GMSMarker(position: dropOffCoordinate)
        dropMarker.icon = #imageLiteral(resourceName: "marker_green")
        dropMarker.isTappable = true
        dropMarker.map = self.mapView

        
        
    }
    
    
    
    func isLocationWithinScreen(position: CLLocationCoordinate2D) -> Bool {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(position)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation {
            let myLocation: CLLocation =  change![NSKeyValueChangeKey.newKey] as! CLLocation
            didFindMyLocation = true
            currentLocation = myLocation
        }
    }
    
    func moveCarMarker(previouslat:Double,previousLong:Double, currentLocation: CLLocation){
        self.moveMent.arCarMovement(self.carMarker, withOldCoordinate: CLLocationCoordinate2D.init(latitude: previouslat, longitude: previousLong), andNewCoordinate: CLLocationCoordinate2D.init(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), inMapview: self.mapView, withBearing: 0.0)
        self.previousLocation = currentLocation
        self.previousLatitude = currentLocation.coordinate.latitude
        self.previousLongitude = currentLocation.coordinate.longitude
    }
    
    func arCarMovement(_ movedMarker: GMSMarker) {
        carMarker = movedMarker
        carMarker.map = mapView
        
    }
    
    @IBAction func onClickLocationButton(_ sender: UIButton){
        guard let location = self.currentLocation else {return}
        self.mapView.animate(toLocation: location.coordinate)
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15.0, bearing: 0, viewingAngle: 0)
    }
    
    
    func locationUpdate(){
        if self.currentLocation != nil{
            if self.previousLocation != self.currentLocation{
                if self.isLocationWithinScreen(position: self.currentLocation.coordinate){
                    self.moveCarMarker(previouslat: self.previousLatitude, previousLong: self.previousLongitude, currentLocation: self.currentLocation)
                }else{
                    self.animateMapToCenter(position: self.currentLocation.coordinate)
                }
                
            }
            
        }
    }
}


extension ServiceOrderDetailViewController{
    
    func getCurrentDistace(lastLocation:CLLocation){
        let dlat = String(format: "%.10f",lastLocation.coordinate.latitude)
        let dlong = String(format: "%.10f",lastLocation.coordinate.longitude)
        let lat = Double(dlat) ?? 0.0
        let long = Double(dlong) ?? 0.0
        self.sendLocationToFirebase(lat: lat, long: long)
        
    }
    
    
    
    
    func sendLocationToFirebase(lat:Double, long:Double){
        if !CommonClass.sharedInstance.isLoggedIn{return}
        if !isStart{return}
        if isJobComplete{return}
        let messageItem = ["lat": lat,"long": long]
        if let handle = self.rootHandle{
            self.ref.removeObserver(withHandle: handle)
        }
        
        self.rootHandle = self.ref.child("driverjob").observe(.childAdded) { (snapshot) in
            let groupName = snapshot.key

            if groupName == "order_c_\(self.order.orderId)"{
                self.ref.child("driverjob").child(groupName).child("location").setValue(messageItem)
            }else if groupName == "order_d_\(self.order.orderId)"{
                self.ref.child("driverjob").child(groupName).child("location").setValue(messageItem)

            }
        }
    }

    
    func removeDriverFromFirebase(){
        self.driverRemoveRef = Database.database().reference()
        self.driverRemoveRef.keepSynced(false)
        self.newDriverRemoveHandle = self.driverRemoveRef.child("driverjob").observe(.childAdded, with: { (snapshot) in
            let driver = snapshot.key
            if driver != "order_c_\(self.order.orderId)" {
                    let deleteDriver = self.driverRemoveRef.child(driver).child("location")
                    deleteDriver.removeValue()
            }else if driver != "order_d_\(self.order.orderId)" {
                let deleteDriver = self.driverRemoveRef.child(driver).child("location")
                deleteDriver.removeValue()
            }
        })
        
    }


}





// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
