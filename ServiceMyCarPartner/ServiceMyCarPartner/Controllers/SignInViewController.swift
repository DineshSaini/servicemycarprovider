//
//  SignInViewController.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var mobileContainerView:UIView!
    @IBOutlet weak var passwordContainerView:UIView!

    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        CommonClass.makeViewCircularWithRespectToHeight(mobileContainerView, borderColor: .lightGray, borderWidth: 1.0)
        CommonClass.makeViewCircularWithRespectToHeight(passwordContainerView, borderColor: .lightGray, borderWidth: 1.0)

        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func onClickLoginButton(_ sender:UIButton){
        
        guard let email = emailTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your email address!")
            return
        }
        guard let password = passwordTextField.text else {
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message: "Please enter your password!")
            return
        }
        let validationResult = self.validateParams(email, password: password)
        if !validationResult.success{
            showAlertWith(self, message: validationResult.message, title: ALERT_TITTLE_MESSAGE)
            return
        }
        
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        self.loginWith(email: email, password: password)
    }
    
    
    
    func loginWith(email:String,password:String){
        if !CommonClass.isConnectedToNetwork{
            NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:NETWORK_NOT_CONNECTED_MESSAGE)
            return
        }
        CommonClass.showLoader(withStatus: "please wait..")
        AppServices.sharedInstance.loginWith(eamil: email, password: password) { (success, result, message) in
            CommonClass.hideLoader()
                if let responseUser = result, responseUser.userID != ""{
                    AppDelegate.getAppDelegate().getLoggedInUser()
                    kUserDefaults.set(true, forKey: kIsLoggedIN)
                    let menu = AppStoryboard.Dashboard.viewController(LeftMenuViewController.self)
                    let nav = AppStoryboard.Dashboard.viewController(SlideNavigationController.self)
                    nav.leftMenu = menu
                    nav.enableSwipeGesture = true
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
            }else{
                NKToastHelper.sharedInstance.showAlert(self, title: warningMessage.title, message:message)
            }
        }
    }
    
    
    func validateParams(_ email:String,password:String) -> (success:Bool,message:String) {
        if email == ""{
            return (success:false,message:"Enter your Email ID !")
            
        }
        if !CommonClass.validateEmail(email){
        return (success:false,message:"Email is not correct!")
        }
        if password == ""{
            return (success:false,message:"Fill your Password!")
        }
        
        if password.count <= 5{
            return (success:false,message:"Password should be minimum 6 character!")
        }
        
        if !CommonClass.validatePassword(password){
            return (success:false,message:"Password is incorrect!")
        }
        return (success:true,message:"")
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
