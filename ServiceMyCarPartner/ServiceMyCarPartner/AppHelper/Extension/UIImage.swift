//
//  UIIMage.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit

extension UIImage {
//    func trim(trimRect trimRect :CGRect) -> UIImage {
//        if CGRectContainsRect(CGRect(origin: CGPointZero, size: self.size), trimRect) {
//            if let imageRef = CGImageCreateWithImageInRect(self.CGImage, trimRect) {
//                return UIImage(CGImage: imageRef)
//            }
//        }
//        
//        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
//        self.drawInRect(CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
//        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        guard let image = trimmedImage else { return self }
//        
//        return image
//    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}

