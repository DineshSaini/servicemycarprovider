//
//  AppSettings.swift
//  FoodClubDeliveryBoy
//
//  Created by admin on 07/05/18.
//  Copyright © 2018 Tecorb. All rights reserved.
//

import UIKit
import CoreFoundation
import SystemConfiguration

class AppSettings{
    
    static let shared = AppSettings()
    fileprivate init() {}
    
    func prepareHeader(withAuth:Bool) -> Dictionary<String,String>{
        let accept = "application/json"
        let currentVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
        var header = Dictionary<String,String>()
//        if withAuth{
//            let user = User.loadSavedUser()
//            let userToken = user.apiKey
//            header.updateValue(userToken, forKey: "accessToken")
//        }
        
        header.updateValue(currentVersion, forKey: "currentVersion")
       // header.updateValue("ios", forKey: "varDeviceOS")
        header.updateValue(accept, forKey: "Accept")
        header.updateValue("smc1989", forKey: "X-API-KEY")
        return header
    }
    
    
    func showSessionExpireAndProceedToLandingPage(){
        NKToastHelper.sharedInstance.showErrorAlert(nil, message: warningMessage.sessionExpired.rawValue, completionBlock: {
            self.proceedToLandingPage()
        })
    }
    
    func showForceUpdateAlert(){
        let alert = UIAlertController(title: warningMessage.title.rawValue, message: warningMessage.updateVersion.rawValue, preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Update Now", style: .cancel) {[alert] (action) in
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)"),
                UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, options: [:], completionHandler: {[alert] (done) in
                    alert.dismiss(animated: false, completion: nil)
                })
            }
        }
        alert.addAction(updateAction)
        let toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        toastShowingVC?.present(alert, animated: true, completion: nil)
    }
    
    
    func proceedToLandingPage(){
        kUserDefaults.set(false, forKey: kIsLoggedIN)
        SlideNavigationController.sharedInstance().closeMenu {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController = nav
        }
        
    }
    
 /*   var currentCountryCode:String{
        let networkInfo = CTTelephonyNetworkInfo()
        if let carrier = networkInfo.subscriberCellularProvider{
            let countryCode = carrier.isoCountryCode
            return countryCode ?? "US"
        }else{
            return Locale.autoupdatingCurrent.regionCode ?? "US"
        }
    }
    
    var previousRatedVersion : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPreviousRatedVersion) as? String{
                result = r
            }
            return result
        }
        set(newpreviousRatedVersion){
            kUserDefaults.set(newpreviousRatedVersion, forKey: kPreviousRatedVersion)
        }
    }

    //cedential
    
    var phoneNumber : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kPhoneNumber) as? String{
                result = r
            }
            return result
        }
        set(newPhoneNumber){
            kUserDefaults.set(newPhoneNumber, forKey: kPhoneNumber)
        }
    }
    
    var countryCode : String {
        get {
            var result = ""
            if let r = kUserDefaults.value(forKey:kCountryCode) as? String{
                result = r
            }
            return result
        }
        set(newCountryCode){
            kUserDefaults.set(newCountryCode, forKey: kCountryCode)
        }
    }*/

}

extension UIApplication {
    
    class func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func appBuild() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    //
    
}
