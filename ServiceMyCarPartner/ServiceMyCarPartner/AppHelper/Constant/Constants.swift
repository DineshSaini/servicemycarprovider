//
//  Constants.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import JDStatusBarNotification
import TSMessages

let kNavigationColor = UIColor(red:50.0/255.0, green:58.0/255.0, blue:71.0/255, alpha:1.0)
let kApplicationRedColor = UIColor(red:250.0/255.0, green:99.0/255.0, blue:98.0/255, alpha:1.0)

let warningMessageShowingDuration = 1.25

/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
   
    public static let UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UpdateProfileNotification")
    public static let UPDATE_ORDER_COUNT_NOTIFICATION = NSNotification.Name("UpdateOrderCountNotification")
    
    public static let TRANPORTER_ASSIGNED_NOTIFICATION = NSNotification.Name("TransporterAssignedNotification")
    public static let TRANPORTER_REMOVED_NOTIFICATION = NSNotification.Name("TransporteRemovedNotification")
    public static let APPDELEGATE_UPDATE_LOCATION_NOTIFICATION = NSNotification.Name("AppdelegateUpdateLocationNotification")



}

/*====================== App Colors ======================*/

let appBackGroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)




let kNotificationType = "notification_type"
let kTransporterAssignedNotification = "transporter_assigned"
let kTransporterRemove = "transporter_remove"



let kUserDefaults = UserDefaults.standard

/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/


let kIsLoggedIN = "is_logged_in"
let kUserName = "user_name"
let kPassword = "password"
let kUserEmail = "email"
let kUser = "user"
let KuserID = "user_id"
let kSecurityToken = "access_token"
let kDeviceToken = "device_type"
let appID = ""
let kUserStatus = "user_status"


/*============= Error Code ===================*/

enum ErrorCode:Int{
    case success
    case failure
    case forceUpdate
    case sessionExpire
    
    init(rawValue:Int) {
        if rawValue == 102{
            self = .forceUpdate
        }else if rawValue == 345{
            self = .sessionExpire
        }else if ((rawValue == 2001)/* && (rawValue < 300)*/){
            self = .success
        }else{
            self = .failure
        }
    }
}


/*================== API URLs ====================================*/
let BASE_URL = isLive ? "https://servicemycar.ae/smc/web_api_v2/" :"https://servicemytaxi.com/smc/web_api/"//"https://servicemytaxi.com/"
let API_VERSION = "web_api/"

let isLive = true

enum api: String {
    case base
    case login
    case bookingList
    
    func url() -> String {
        switch self {
        case.base : return BASE_URL
        case.login : return "\(BASE_URL)driverLogin"
        case.bookingList : return "\(BASE_URL)getDriverBookingJobs"


        }
    }
}


/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "facebook"
    case google = "google"
}

/*======================== CONSTANT MESSAGES ==================================*/

let NETWORK_NOT_CONNECTED_MESSAGE = "Network is not connected!"
let FUNCTIONALITY_PENDING_MESSAGE = "Under Development. Please ignore it!"
let ALERT_TITTLE_MESSAGE = "ServiceMyCar Provider"


/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let SELF_URL_SCHEME = "com.ServiceMyCarProvider"
let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.1077906047027-e9up7gp3jjv0pq6tfa244n84u94sva1l"

/*============== PRINTING IN DEBUG MODE ==================*/

func print_debug <T>(_ object : T){
    print(object)
}

func print_log <T>(_ object : T){
//    NSLog("\(object)")
}





/*============== SHOW MESSAGE ==================*/


func showSuccessWithMessage(_ message: String)
{
    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleSuccess)

    // TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.success)
}
func showErrorWithMessage(_ message: String)
{
    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleError)
    //TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.error)
}
func showWarningWithMessage(_ message: String)
{
   // JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleWarning)
    TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.warning)
}
func showMessage(_ message: String)
{
   // JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleDefault)
    TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.message)
}


func showAlertWith(_ viewController: UIViewController,message:String,title:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
        alert.dismiss(animated: true, completion: nil)
    }
    alert.addAction(okayAction)
    viewController.present(alert, animated: true, completion: nil)
}





