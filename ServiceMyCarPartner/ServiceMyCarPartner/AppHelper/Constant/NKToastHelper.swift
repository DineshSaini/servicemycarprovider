//
//  NKToastHelper.swift
//  TinNTap
//
//  Created by TecOrb on 16/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Toast
/*============== SHOW MESSAGE ==================*/
enum ToastPosition {
    case top,center,bottom
}
enum warningMessage : String{
    case title = "Important Message"
    case updateVersion = "You are using a version of Food Club Business that\'s no longer supported. Please upgrade your app to the newest app version to use Food Club Business . Thanks!"
    case sessionExpired = "Your session has expired, Please login again"
}

class NKToastHelper {
    let duration : TimeInterval = 1.5
    static let sharedInstance = NKToastHelper()
    fileprivate init() {}

    func showToastWithViewController(_ viewController: UIViewController?,message: String, position:ToastPosition,completionBlock:@escaping () -> Void){
        var toastPosition : String!
        var toastShowingVC :UIViewController!

        if let vc = viewController{
            toastShowingVC = vc
        }else{
            toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        }
        switch position {
        case .top:
            toastPosition = CSToastPositionTop
        case .center :
            toastPosition = CSToastPositionCenter
        case .bottom:
            toastPosition = CSToastPositionBottom
        }
        toastShowingVC.view.makeToast(message, duration: duration, position: toastPosition)
        completionBlock()
    }

    func showSuccessAlert(_ viewController: UIViewController?, message: String,completionBlock :(() -> Void)? = nil){
        self.showAlertWithViewController(viewController, title: warningMessage.title, message: message) {
            completionBlock?()
        }
    }
    
    func showAlert(_ viewController: UIViewController?,title:warningMessage, message: String,completionBlock :(() -> Void)? = nil){
        self.showAlertWithViewController(viewController, title: title, message: message) {
            completionBlock?()
        }
    }

    func showErrorAlert(_ viewController: UIViewController?, message: String, completionBlock :(() -> Void)? = nil){
        self.showAlertWithViewController(viewController, title: warningMessage.title, message: message) {
            completionBlock?()
        }
    }


    //complitionBlock : ((_ done: Bool) ->Void)? = nil
    private func showAlertWithViewController(_ viewController: UIViewController?, title: warningMessage, message: String,completionBlock :(() -> Void)? = nil){
        var toastShowingVC :UIViewController!

        if let vc = viewController{
            toastShowingVC = vc
        }else{
            toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        }
        let alert = UIAlertController(title: title.rawValue, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            guard let handler = completionBlock else{
                alert.dismiss(animated: false, completion: nil)
                return
            }
            handler()
            alert.dismiss(animated: false, completion: nil)
        }
        alert.addAction(okayAction)
        toastShowingVC.present(alert, animated: true, completion: nil)
    }



    func showErrorToast(message:String,completionBlock:@escaping () -> Void){

    }

}
