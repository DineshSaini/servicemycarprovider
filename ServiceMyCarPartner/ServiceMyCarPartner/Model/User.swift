//
//  User.swift
//  FoodClubDeliveryBoy
//
//  Created by admin on 13/05/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    //key
    let kUserID = "UserId"
    let kDriverID = "DriverId"
    let kUserLoginEmail = "UserLoginEmail"
    let kUserName = "UserName"
    let kValidated = "Validated"
    //properties
    
    var userID = ""
    var driverID = ""
    var userLoginEmail = ""
    var userName = ""
    var validated = false
    
    
    override init() {
        super.init()
    }
    
    init(json : JSON){
        if let _UserID = json[kUserID].string as String?{
            self.userID = _UserID
        }else if let _UserID = json[kUserID].int as Int?{
            self.userID = "\(_UserID)"
        }
        
        if let _DriverID = json[kDriverID].string as String?{
            self.driverID = _DriverID
        }else if let _DriverID = json[kDriverID].int as Int?{
            self.driverID = "\(_DriverID)"
        }
        
        if let _UserEmail = json[kUserLoginEmail].string as String?{
            self.userLoginEmail = _UserEmail
         }
        if let _UserName = json[kUserName].string as String?{
            self.userName = _UserName
        }
        if let _ValidUser = json[kValidated].bool as Bool?{
            self.validated = _ValidUser
        }

        
        super.init()
    }
    
    init(dict : [String:AnyObject]){
        if let _UserID = dict[kUserID] as? String{
            self.userID = _UserID
        }else if let _UserID = dict[kUserID] as? Int{
            self.userID = "\(_UserID)"
        }
        
        if let _DriverID = dict[kDriverID] as? String{
            self.driverID = _DriverID
        }else if let _DriverID = dict[kDriverID] as? Int{
            self.driverID = "\(_DriverID)"
        }
        
        if let _UserEmail = dict[kUserLoginEmail] as? String{
            self.userLoginEmail = _UserEmail
        }
        if let _UserName = dict[kUserName] as? String{
            self.userName = _UserName
        }
        if let _ValidUser = dict[kValidated] as? Bool{
            self.validated = _ValidUser
        }
        

        
        super.init()
    }
    
    
//    class func deleteUserOnLogout(){
//        let emptyUser = ["data":""]
//
//        do {
//            let data = try JSON(emptyUser).rawData(options: [.prettyPrinted])
//            let documentPath = NSHomeDirectory() + "/Documents/"
//            let path = documentPath + "data"
//            try data.write(to: URL(fileURLWithPath: path), options: .atomic)
//        }catch{
//            print_debug("error in saving userinfo")
//        }
//    }
    
    func saveUserJSON(_ json:JSON) {
        if let userInfo = json["data"].dictionaryObject as [String:AnyObject]?{
            let documentPath = NSHomeDirectory() + "/Documents/"
            do {
                let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
                let path = documentPath + "data"
                try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            }catch{
                print_debug("error in saving userinfo")
            }
            UserDefaults.standard.synchronize()
        }
    }

    
    class func loadSavedUser() -> User {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "data"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = try JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting userinfo")
        }
        
        let user = User(json: json)
        return user
    }
}

class UserParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "errors"
    let kUsers = "data"
    let kOrders = "data"
    let kUserStatus = "status"
    var errorCode = ErrorCode.failure
    var responseCode = ""
    var message = ""
    var users = User()
    var orders = [Order]()
    var webReport = Order()
    var userStatus:String = ""
    
    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.errorCode = ErrorCode(rawValue: _rCode)
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["response_message"].string as String?{
            self.message = _rMessage
        }else if let _rMessage = json["msg"].string as String?{
            self.message = _rMessage
        }
        if let userDict = json[kUsers].dictionaryObject as [String:AnyObject]?{
            self.users = User(dict: userDict)
        }
        if let _UserStatus = json[kUserStatus].string as String?{
            self.userStatus = _UserStatus
        }
        if let _orders = json[kOrders].arrayObject as? [[String:AnyObject]]{
            self.orders.removeAll()
            for ord in _orders{
                let order = Order(dict: ord)
                self.orders.append(order)
            }
        }
        if let _webReport = json[kUsers].dictionaryObject as [String:AnyObject]?{
            self.webReport = Order(dict: _webReport)
        }
    }
    
}

