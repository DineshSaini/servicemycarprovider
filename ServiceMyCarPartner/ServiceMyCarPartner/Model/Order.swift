//
//  Order.swift
//  ServiceMyCarPartner
//
//  Created by admin on 28/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import SwiftyJSON

class Order: NSObject {
    
    
    let kIntId = "intId"
    let kOrderId = "orderId"
    let kServiceType = "serviceType"
    let kJobType = "jobType"
    let kJobTypeTxt = "jobTypeTxt"
    let kTripStatus = "tripStatus"
    let kBookingType = "bookingType"
    let kAddress = "address"
    let kLatLang = "latLang"
    let kDateTime = "dateTime"
    let kVarCar = "varCar"
    let kVarModel = "varModel"
    let kVarYear = "varYear"
    let kVarPackageType = "varPackageType"
    let kCustomerName = "customerName"
    let kCustomerPhone = "customerPhone"
    let kAdminPhone = "adminPhone"
    let kUrl = "url"
    let kStartLat = "startLat"
    let kStartLong = "startLong"
    let kDtTripStart = "dtTripStart"
    let kDtTripEnd = "dtTripEnd"
    
   
    
    
    var intId:String = ""
    var orderId:String = ""
    var serviceType:String = ""
    var jobType:String = ""
    var jobTypeTxt:String = ""
    var tripStatus:String = ""
    var bookingType:String = ""
    var address:String = ""
    var latLang:String = ""
    var dateTime = ""
    var varCar:String = ""
    var varModel:String = ""
    var varYear:String = ""
    var varPackageType:String = ""
    var customerName:String = ""
    var customerPhone:String = ""
    var adminPhone:String = ""
    var url = ""
    
    var startLat = ""
    var startLong = ""
    var dtTripStart = ""
    var dtTripEnd = ""

    
    
    override init() {
        super.init()
    }
    
    init(json : JSON){
        if let _ID = json[kIntId].string as String?{
            self.intId = _ID
        }else if let _ID = json[kIntId].int as Int?{
            self.intId = "\(_ID)"
        }
        if let _orderID = json[kOrderId].string as String?{
            self.orderId = _orderID
        }else if let _orderID = json[kOrderId].int as Int?{
            self.orderId = "\(_orderID)"
        }
        if let _ServiceType = json[kServiceType].string as String?{
            self.serviceType = _ServiceType
        }
        if let _JobType = json[kJobType].string as String?{
            self.jobType = _JobType
        }
        if let _JobTypetext = json[kJobTypeTxt].string as String?{
            self.jobTypeTxt = _JobTypetext
        }
        if let _tripStaus = json[kTripStatus].string as String?{
            self.tripStatus = _tripStaus
        }
        if let bookingType = json[kBookingType].string as String?{
            self.bookingType = bookingType
        }
        if let _Address = json[kAddress].string as String?{
            self.address = _Address
        }
        if let _LatLong = json[kLatLang].string as String?{
            self.latLang = _LatLong
        }
        if let _Date = json[kDateTime].string as String?{
            self.dateTime = _Date//CommonClass.dateWithString(_Date)
        }
        if let _varCar = json[kVarCar].string as String?{
            self.varCar = _varCar
        }
        if let _VarModel = json[kVarModel].string as String?{
            self.varModel = _VarModel
        }
        if let _varYear = json[kVarYear].string as String?{
            self.varYear = _varYear
        }
        if let _VarPackageType = json[kVarPackageType].string as String?{
            self.varPackageType = _VarPackageType
        }
        if let _customerName = json[kCustomerName].string as String?{
            self.customerName = _customerName
        }
        if let _customerPhone = json[kCustomerPhone].string as String?{
            self.customerPhone = _customerPhone
        }
        if let _adminPhone = json[kAdminPhone].string as String?{
            self.adminPhone = _adminPhone
        }
        if let _Url = json[kUrl].string as String?{
            self.url = _Url
        }
        if let startlatitude = json[kStartLat].string as String?{
            self.startLat = startlatitude
        }else  if let startlatitude = json[kStartLat].double as Double?{
            self.startLat = "\(startlatitude)"
        }
        if let startLongitude = json[kStartLong].string as String?{
            self.startLong = startLongitude
        }else  if let startLongitude = json[kStartLong].double as Double?{
            self.startLong = "\(startLongitude)"
        }
        if let dateTripStart = json[kDtTripStart].string as String?{
            self.dtTripStart = dateTripStart
        }
        if let dateTripEnd = json[kDtTripEnd].string as String?{
            self.dtTripEnd = dateTripEnd
        }
        super.init()
    }
    
    init(dict : [String:AnyObject]){
        
        if let _ID = dict[kIntId] as? String{
            self.intId = _ID
        }else if let _ID = dict[kIntId] as? Int{
            self.intId = "\(_ID)"
        }
        if let _orderID = dict[kOrderId] as? String{
            self.orderId = _orderID
        }else if let _orderID = dict[kOrderId] as? Int{
            self.orderId = "\(_orderID)"
        }
        if let _ServiceType = dict[kServiceType] as? String{
            self.serviceType = _ServiceType
        }
        if let _JobType = dict[kJobType] as? String{
            self.jobType = _JobType
        }
        if let _JobTypetext = dict[kJobTypeTxt] as? String{
            self.jobTypeTxt = _JobTypetext
        }
        if let _tripStaus = dict[kTripStatus] as? String{
            self.tripStatus = _tripStaus
        }
        if let bookingType = dict[kBookingType] as? String{
            self.bookingType = bookingType
        }
        if let _Address = dict[kAddress] as? String{
            self.address = _Address
        }
        if let _LatLong = dict[kLatLang] as? String{
            self.latLang = _LatLong
        }
        if let _Date = dict[kDateTime] as? String{
            self.dateTime = _Date//CommonClass.dateWithString(_Date)
        }
        if let _varCar = dict[kVarCar] as? String{
            self.varCar = _varCar
        }
        if let _VarModel = dict[kVarModel] as? String{
            self.varModel = _VarModel
        }
        if let _varYear = dict[kVarYear] as? String{
            self.varYear = _varYear
        }
        if let _VarPackageType = dict[kVarPackageType] as? String{
            self.varPackageType = _VarPackageType
        }
        if let _customerName = dict[kCustomerName] as? String{
            self.customerName = _customerName
        }
        if let _customerPhone = dict[kCustomerPhone] as? String{
            self.customerPhone = _customerPhone
        }
        if let _adminPhone = dict[kAdminPhone] as? String{
            self.adminPhone = _adminPhone
        }
        if let _Url = dict[kUrl] as? String{
            self.url = _Url
        }
        if let startlatitude = dict[kStartLat] as? String{
            self.startLat = startlatitude
        }else  if let startlatitude = dict[kStartLat] as? Double{
            self.startLat = "\(startlatitude)"
        }
        if let startLongitude = dict[kStartLong] as? String{
            self.startLong = startLongitude
        }else  if let startLongitude = dict[kStartLong] as? Double{
            self.startLong = "\(startLongitude)"
        }
        if let dateTripStart = dict[kDtTripStart] as? String{
            self.dtTripStart = dateTripStart
        }
        if let dateTripEnd = dict[kDtTripEnd] as? String{
            self.dtTripEnd = dateTripEnd
        }

        super.init()
    }


}
