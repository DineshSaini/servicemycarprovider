//
//  ServiceTableViewCell.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var serviceType:UILabel!
    @IBOutlet weak var orderID:UILabel!
    @IBOutlet weak var jobType:UILabel!
    @IBOutlet weak var status:UILabel!
    @IBOutlet weak var dateAndTime:UILabel!
    @IBOutlet weak var address:UILabel!
    
    @IBOutlet weak var cellContainer:UIView!
    @IBOutlet weak var statusView:UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CommonClass.makeViewCircularWithCornerRadius(cellContainer, borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
