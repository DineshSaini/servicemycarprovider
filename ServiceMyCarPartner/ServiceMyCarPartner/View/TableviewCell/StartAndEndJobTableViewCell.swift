//
//  StartAndEndJobTableViewCell.swift
//  ServiceMyCarPartner
//
//  Created by admin on 28/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit

class StartAndEndJobTableViewCell: UITableViewCell {

    @IBOutlet weak var startJobButton:UIButton!
    @IBOutlet weak var endJobButton:UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
