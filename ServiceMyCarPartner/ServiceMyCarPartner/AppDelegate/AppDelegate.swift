//
//  AppDelegate.swift
//  ServiceMyCarPartner
//
//  Created by admin on 27/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import GooglePlaces
import GoogleMaps


let googleApiKey = "AIzaSyC4_bhTW7L0-uoV35GxuZy2yEc-XsI-fV0"//"AIzaSyB4meCBM-nlxYHtpGwQMc3mTxVyqrsX4OY"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    var ref: DatabaseReference!
    //Mark:- Ride request handler and reference
    var rideRequestRef: DatabaseReference!
    var newRideRequestHandle: DatabaseHandle?
    
    
    var changedRideRequestHandle: DatabaseHandle?
    var removeRideRequestHandle: DatabaseHandle?
    
    
    
    
    var _refHandle: DatabaseHandle!
    var rootHandle: DatabaseHandle?
    var childHandle: DatabaseHandle!
    
    var user: User!
    let locationManager = CLLocationManager()
    var currentLocation : CLLocation!
    
    func getLoggedInUser(){
        self.user = User.loadSavedUser()
        
    }
    
    func referenceHandle(){
        if let refHandle = _refHandle {
            self.ref.removeObserver(withHandle: refHandle)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        self.getLoggedInUser()
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
        FirebaseApp.configure()
        self.registerForRemoteNotification()
        
        self.referenceHandle()
        Database.database().isPersistenceEnabled = false
        self.ref = Database.database().reference()
        self.ref.keepSynced(false)
        self.updateLocationInBackground()
        
        if CommonClass.sharedInstance.isLoggedIn{
            let menu = AppStoryboard.Dashboard.viewController(LeftMenuViewController.self)
            let nav = AppStoryboard.Dashboard.viewController(SlideNavigationController.self)
            nav.leftMenu = menu
            nav.enableSwipeGesture =  false
            nav.avoidSwitchingToSameClassViewController = false
            self.window?.rootViewController = nav
        }else{
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
            self.window?.rootViewController = nav
        }
        return true
    }
    
    
    
    func updateLocationInBackground(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        //locationManager.allowsBackgroundLocationUpdates = true
        
        //updated by Nakul
        locationManager.pausesLocationUpdatesAutomatically = false
        //End of update
        //locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        //self.startReceivingSignificantLocationChanges()
        locationManager.distanceFilter = 0
        
    }
    
    
    func registerForRemoteNotification() {
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
            print_debug("registered to firebase with token : \(Messaging.messaging().fcmToken ?? "--")")
        }else{
            print_debug("already registered to firebase with token : \(kUserDefaults.value(forKey: kDeviceToken) ?? "--")")
        }
        
    }
    
    
    //******************=================== Notification Handling Start ===========================*********************//
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
       
    }
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    
    func openNotification(_ application:UIApplication,userInfoDict:Dictionary<String,AnyObject>){
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print_debug("userInfo: \(userInfo)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print_debug("userInfo: \(userInfo)")
    }
    
    //******************=================== Notification Handling End ===========================*********************//

    

    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "ServiceMyCarPartner")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate : MessagingDelegate,CLLocationManagerDelegate {
    
    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            kUserDefaults.set(refreshedToken, forKey: kDeviceToken)
            print_debug("InstanceID token: \(refreshedToken)")
        }
        
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print_debug("remote message received: \(remoteMessage)")
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
        case .authorizedAlways:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
        case .restricted:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: nil) { (done) in
                
            }
        case .denied:
            CommonClass.sharedInstance.showGotoLocationSettingAlert(in: nil) { (done) in
            }
            
        }
    }
    
    
    func startReceivingSignificantLocationChanges() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedWhenInUse {
            return
        }
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        NotificationCenter.default.post(name: .APPDELEGATE_UPDATE_LOCATION_NOTIFICATION, object: nil, userInfo: ["currentLocation":locations.last as Any])
        
        if let location = locations.last{
            self.currentLocation = location
            NotificationCenter.default.post(name: .APPDELEGATE_UPDATE_LOCATION_NOTIFICATION, object: nil, userInfo: ["currentLocation":location])
        }
    }
}

extension AppDelegate{
    class func getAppDelegate()->AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    class func getDatabaseRef()->DatabaseReference{
        return (UIApplication.shared.delegate as! AppDelegate).ref
    }
    class func locationManager() -> CLLocationManager{
        return (UIApplication.shared.delegate as! AppDelegate).locationManager
    }
}


