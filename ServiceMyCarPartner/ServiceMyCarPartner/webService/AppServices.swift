//
//  AppServices.swift
//  ServiceMyCarPartner
//
//  Created by admin on 28/06/19.
//  Copyright © 2019 Tecorb. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AppServices {
    static let sharedInstance = AppServices()
    fileprivate init() {}
    
    
    func loginWith(eamil:String,password:String,completionBlock:@escaping (_ success:Bool, _  user: User?, _ message: String) -> Void){
        
        var params = Dictionary<String,String >()
        params.updateValue(eamil, forKey: "varLoginEmail")
        params.updateValue(password, forKey: "varLoginPassword")
        params.updateValue("IOS", forKey: "varDeviceOS")
        if let deviceID = kUserDefaults.value(forKey: kDeviceToken) as? String{
            params.updateValue(deviceID, forKey: "varDeviceId")
        }else{
            params.updateValue("--", forKey: "varDeviceId")
        }
        
       // let head =  AppSettings.shared.prepareHeader(withAuth: false)
        let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]
        print_debug("hitting \(api.login.url()) with param \(params) and headers :\(header)")
        CommonClass.showLoader(withStatus: "Authenticating..")
        Alamofire.request(api.login.url(),method: .post,parameters: params, encoding: URLEncoding.default, headers: header as? HTTPHeaders).responseString
            { response in
               // print(dicsParams)
                if((response.result.error) != nil)
                {
               
                }
            }
            .responseString(completionHandler: { (string) in
                
            }).responseJSON {response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Login in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                        if userParser.userStatus == "success"{
                            userParser.users.saveUserJSON(json)
                            kUserDefaults.set(true, forKey: kIsLoggedIN)
                            completionBlock((userParser.errorCode == .success),userParser.users,userParser.message)
                        }else{
                            completionBlock((userParser.errorCode == .success),userParser.users,userParser.message)
                        }
                  
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    func orderListFromServer(driverId:String,status:String,completionBlock:@escaping (_ success:Bool, _  order: Array<Order>?, _ message: String) -> Void){
        
        var params = Dictionary<String,String >()
        params.updateValue(driverId, forKey: "driverId")
        params.updateValue(status, forKey: "status")
        
        let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]
        let url = "\(BASE_URL)getDriverBookingJobs?driverId=\(driverId)&status=\(status)"
        print_debug("hitting \(api.bookingList.url()) with param \(params) and headers :\(header)")
        Alamofire.request(url,method: .get , headers:header as? HTTPHeaders).responseString
            { response in
                if((response.result.error) != nil)
                {
                    
                }
            }
            .responseString(completionHandler: { (string) in
                
            }).responseJSON {response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Booking list in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                        if userParser.userStatus == "success"{
                            completionBlock((userParser.errorCode == .success),userParser.orders,userParser.message)
                        }else{
                            completionBlock((userParser.errorCode == .success),userParser.orders,userParser.message)
                        }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    func webReportFromServer(orderID:String,type:String,completionBlock:@escaping (_ success:Bool, _  order: Order?, _ message: String) -> Void){
        
      
        
        let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]
        let url = "\(BASE_URL)webReport?orderId=\(orderID)&type=\(type)"
        Alamofire.request(url,method: .get, headers:header as? HTTPHeaders).responseString
            { response in
                if((response.result.error) != nil)
                {
                    
                }
            }
            .responseString(completionHandler: { (string) in
                
            }).responseJSON {response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Web Report list in json is:\n\(json)")
                    let userParser = UserParser(json: json)
                        if userParser.userStatus == "success"{
                            completionBlock((userParser.errorCode == .success),userParser.webReport,userParser.message)
                        }else{
                            completionBlock((userParser.errorCode == .success),userParser.webReport,userParser.message)
                        }
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    
    func endJobFromServer(orderID:String,completionBlock:@escaping (_ success:Bool, AnyObject?, _ message: String) -> Void){
        
       
        
        let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]
        let url = "\(BASE_URL)endTrip?rowId=\(orderID)"
        Alamofire.request(url,method: .put, headers:header as? HTTPHeaders).responseString
            { response in
                // print(dicsParams)
                if((response.result.error) != nil)
                {
                    
                }
            }
            .responseString(completionHandler: { (string) in
                
            }).responseJSON {response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("End job list in json is:\n\(json)")
                        let userParser = UserParser(json: json)
                        if userParser.userStatus == "success"{
                            CommonClass.hideLoader()
                           // NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Job ended Successfully")
                            completionBlock((userParser.userStatus == "success"),userParser.userStatus as AnyObject,userParser.message)

                        }else{
                            CommonClass.hideLoader()
                            NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Please try again")

                        }
                    }else{
                        completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                case .failure(let error):
                    completionBlock(false,nil,error.localizedDescription)
                }
        }
    }
    
    
    
    func startJobFromServer(orderID:String,lat:String,long:String,completionBlock:@escaping (_ success:Bool, AnyObject?, _ message: String) -> Void){
        
      
        let url = "\(BASE_URL)startTrip?rowId=\(orderID)&varStartLatLong=\(lat),\(long)"
        let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]
        Alamofire.request(url,method: .put, headers:header as? HTTPHeaders).responseString
            { response in
                // print(dicsParams)
                if((response.result.error) != nil)
                {
                    
                }
            }
            .responseString(completionHandler: { (string) in
                
            }).responseJSON {response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print_debug("Start job list in json is:\n\(json)")
                        let userParser = UserParser(json: json)
                        if userParser.userStatus == "success"{
                            CommonClass.hideLoader()
                            //NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Job ended Successfully")
                            completionBlock((userParser.userStatus == "success"),userParser.userStatus as AnyObject,userParser.message)
                            
                        }else{
                            CommonClass.hideLoader()
                            NKToastHelper.sharedInstance.showAlert(nil, title: warningMessage.title, message: "Please try again")
                        }
                    }else{
                        completionBlock(false,nil,response.result.error?.localizedDescription ?? "Some thing went wrong")
                    }
                case .failure(let error):
                    completionBlock(false,nil,error.localizedDescription)
                }
        }
    }



}
